[ -f ~/.profile ] && . ~/.profile

# Honor per-interactive-shell startup file
if [ -f ~/.bashrc ]; then . ~/.bashrc; fi
export LESS_TERMCAP_mb='[01;32m'
export LESS_TERMCAP_md='[01;32m'
export LESS_TERMCAP_me='[0m'
export LESS_TERMCAP_se='[0m'
export LESS_TERMCAP_so='[01;47;34m'
export LESS_TERMCAP_ue='[0m'
export LESS_TERMCAP_us='[01;36m'
export LESS=-R
export PATH="$HOME/.local/bin${PATH:+:$PATH}"

. ~/.devspec
[ -f ~/.guix-home/setup-environment ] && . ~/.guix-home/setup-environment
if [[ $(tty) == /dev/tty2 ]]; then
        exec $XDG_CURRENT_DESKTOP
fi