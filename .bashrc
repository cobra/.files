# Source other files
[ -r ~/.prompt ] && . ~/.prompt
[ -r ~/.devspec ] && . ~/.devspec
[ -r ~/.config/colorscheme ] && . ~/.config/colorscheme

_COLOR_GUIX_OUT="${_BLD}${_RED}"
_COLOR_GUIX_OUT_SEP="${_PRP}"

[ "$TERM" == "screen.linux" ] && export TERM="screen"

export EDITOR="vim"
export TERM=xterm-256color

# History stuff
export HISTCONTROL=ignoredups:erasedups
export HISTFILESIZE=1000
export HISTSIZE=1000

export GPG_TTY=$(tty)

export MANPATH=/run/current-system/profile/share/man:/home/cobra/.guix-profile/share/man:/run/current-system/profile/share/man:/home/cobra/.guix-home/profile/share/man

clear() {
        printf '\033c' # faster than ncurses clear by a lot
}

# Other aliases
alias c='clear'
alias ls='ls --color=auto -FAh'
alias ll='ls -l'
alias nf='hyfetch'
alias ga='git add'
alias gc='git commit -S'
alias gp='git push'
alias mpv='mpv --no-audio-display'
alias wv='swayhide mpv $($PASTE_COMMAND)'
alias curl='curl -L'
alias pass='passage'

# Random string (passwords)
randstr() {
        tr -cd '[:graph:]' </dev/urandom | head -c ${1:-256}
}

# Get CPU usage
cpusage() {
        bc<<<"scale=3;$(ps aux | awk 'BEGIN {sum=0} {sum+=$3}; END {print sum}') / $(nproc --all)"
}

# Update and upgrade system
update() { eval "$UPDATE"; }

# Im too lazy to type
copy() {
        printf "%s" "$(</dev/stdin)" > >($COPY_COMMAND)
}

# URL-encode stdin
urlencode() {
        LC_COLLATE_OLD=$LC_COLLATE
        LC_COLLATE=C

        local TEXT="$(</dev/stdin)"
        local LENGTH="${#TEXT}"
        for (( i = 0; i < LENGTH; i++ )); do
                local c="${TEXT:$i:1}"
                case $c in
                        [a-zA-Z0-9.~_-]) printf '%s' "$c" ;;
                        *) printf '%%%02X' "'$c" ;;
                esac
        done
        LC_COLLATE=$LC_COLLATE_OLD
}

diffhome() { find . -type f -regex '\./\.[^g].*' -exec diff -q ~/{} {} 2> /dev/null \;; }

fixdh() {
        diffhome | while read -r _ SRC _ DEST _; do
                cp $SRC $DEST;
        done;
}

strlen() {
        local N="$*"
        echo "${#N}"
}

caption() {
        [ -z "$3" ] && {
                echo "Usage: caption [input] [output] [text...]"
                return 1
        }
        local FILE="$1"
        local OUT="$2"
        shift 2
        local WIDTH="$(vipsheader -f width "$FILE")"
        local HEIGHT="$(vipsheader -f height "$FILE")"
        local SIZE="$((WIDTH/10))"
        vips text /tmp/vipstmptxt0.v "<span background='white'>$*</span>" --font="Noto Sans Bold $SIZE" --width="$((WIDTH-(WIDTH/25*2)))" --rgba=true
        local TEXT_HEIGHT="$(vipsheader -f height /tmp/vipstmptxt0.v)"
        vips gravity /tmp/vipstmptxt0.v /tmp/vipstmptxt1.v centre "$WIDTH" "$((TEXT_HEIGHT+SIZE))" --extend=white
        vips flatten /tmp/vipstmptxt1.v /tmp/vipstmptxt2.v --background=16777215 --max-alpha=255
        vips extract_band /tmp/vipstmptxt2.v /tmp/vipstmptxt3.v 0
        vips join /tmp/vipstmptxt3.v "$FILE" "$OUT" vertical --expand=true --background=16777215
        rm /tmp/vipstmptxt{0..3}.v
}

playcd() {
        mpv --start=00:00:00.0000000 av://libcdio:/dev/cdrom --cache=yes
}

playdvd() {
        mpv --start=00:00:00.0000000 dvd:///dev/sr0 --cache=yes
}

gpginfo() {
        gpg --with-colons --import-options show-only --import
}

lc() {
        fc -nl '' | sed 's/^\s*//'
}

baseto() {
        if [ $# -ge 3 ]; then
                local i="$1"
                local o="$2"
                shift 2
                bc <<< "obase=$o;$(($i#$*))"
        else
                local line i o n
                while read -r line; do
                        read -r i o n <<< "$line"
                        bc <<< "obase=$o;$(($i#$n))"
                done
        fi
}

rebash() { . ~/.bashrc; }
bashrc() { "$EDITOR" ~/.bashrc && rebash; }

reconf() {
        $PRIV -E guix system reconfigure /etc/guix-config/${HOSTNAME,,}.scm "$@"
}

hreconf() {
        guix home reconfigure $HOME/.config/guix/${HOSTNAME,,}.scm "$@"
}

gshpkg() {
        local re='[\(]?\("([^"]*)"'
        if [[ "$GUIX_ENVIRONMENT" ]]; then
                local PACKAGES=($(while read -r line; do
                                [[ "$line" =~ $re ]] && printf '%s ' "${BASH_REMATCH[1]}"
                        done < $GUIX_ENVIRONMENT/manifest))
                echo ${PACKAGES[@]}
        fi
}