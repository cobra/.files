(define-module (cobra home bin)
  #:use-module (guix gexp)
  #:use-module (gnu packages base)
  #:export (%bin:torbrowser
            %bin:suspend-wrap
            %bin:start-pipewire
            %bin:screenshot
            %bin:rotate
            %bin:logout-wofi
            %bin:lock))

(define %bin:torbrowser
  (computed-file "bin-torbrowser"
    #~(begin
        (system (string-append #$coreutils "/bin/cat << \"EOF\" >" #$output "\n" "\
#!/usr/bin/env bash

cd ~/.local/share/torbrowser/tor-browser/
guix shell --check --pure --expression='(list (@@ (gnu packages gcc) gcc-11) \"lib\")' coreutils bash grep sed gcc-toolchain patchelf gtk+ dbus-glib libxt libevent openssl file alsa-lib << EOT
set -x
cd Browser
patchelf --set-interpreter \\$LIBRARY_PATH/ld-linux-x86-64.so.2 firefox.real
patchelf --set-interpreter \\$LIBRARY_PATH/ld-linux-x86-64.so.2 TorBrowser/Tor/tor
LD_LIBRARY_PATH=\\$LIBRARY_PATH ./start-tor-browser -v
EOT" "\nEOF"))
        (chmod #$output #o755))))

(define %bin:suspend-wrap
  (computed-file "bin-suspend-wrap"
    #~(begin
        (system (string-append #$coreutils "/bin/cat << \"EOF\" >" #$output "\n" "\
#!/usr/bin/env bash
loginctl suspend
while grep -v '\\[' /sys/power/state; do
        swaylock
        break
done" "\nEOF"))
        (chmod #$output #o755))))

(define %bin:start-pipewire
  (computed-file "bin-start-pipewire"
    #~(begin
        (system (string-append #$coreutils "/bin/cat << \"EOF\" >" #$output "\n" "\
#!/usr/bin/env bash

pkill pipewire
pkill wireplumber
pipewire &
pipewire-pulse &
wireplumber &" "\nEOF"))
        (chmod #$output #o755))))

(define %bin:screenshot
  (computed-file "bin-screenshot"
    #~(begin
        (system (string-append #$coreutils "/bin/cat << \"EOF\" >" #$output "\n" "\
#!/usr/bin/env bash

if [ ! -d $HOME/Pictures/Screenshots ]
then
        mkdir -p $HOME/Pictures/Screenshots
fi

fullscreen() {
        filename=$HOME/Pictures/Screenshots/$(date +%Y%m%d_%H:%M:%S_Fullscreen).png
        grim $filename
        cat $filename | wl-copy \
        && notify-send -i $filename \"Fullscreen Screenshot taken\"
        }

selecting() {
        filename=$HOME/Pictures/Screenshots/$(date +%Y%m%d_%H:%M:%S_Selected).png
        grim -g \"$(slurp -b e3eaf699)\" $filename \
        && notify-send -i $filename \"Selective Screenshot taken\"
        cat $filename | wl-copy
        }

wofirun() {
        case \"$(printf \"fullscreen\\nselect\\n\" | wofi --dmenu -i -H 500 -W 250 -x 0 -y 0 )\" in
                'fullscreen')fullscreen;;
                'select')selecting;;
        esac
}

helpscreen() {
        echo \"Usage: screenshot [-f] [-s] [-w] [-h] \"
        echo \"Options:\"
        echo \"-f : Fullscreen screenshot\"
        echo \"-s : Select a region for the screenshot\"
        echo \"-w : Shows a wofi prompt for screenshot\"
        echo \"-h : Shows this fucking help menu\"
        echo \"If no flags are selected then -f is chosen\"
}
case $1 in
        -f)fullscreen;;
        -s)selecting;;
        -h)helpscreen;;
        -w)wofirun;;
        *)fullscreen;;
esac" "\nEOF"))
        (chmod #$output #o755))))

(define %bin:rotate
  (computed-file "bin-rotate"
    #~(begin
        (system (string-append #$coreutils "/bin/cat << \"EOF\" >" #$output "\n" "\
if [ ! -e $XDG_RUNTIME_DIR/rotate.lock ]; then
        swaymsg output LVDS-1 transform 90 clockwise
        touch $XDG_RUNTIME_DIR/rotate.lock
else
        swaymsg output LVDS-1 transform 90 anticlockwise
        rm $XDG_RUNTIME_DIR/rotate.lock
fi"))
        (chmod #$output #o755))))

(define %bin:logout-wofi
  (computed-file "bin-logout-wofi"
    #~(begin
        (system (string-append #$coreutils "/bin/cat << \"EOF\" >" #$output "\n" "\
#!/usr/bin/env bash

case \"$(printf \"logout\\nreboot\\nshutdown\\nsleep\\n\" | wofi --dmenu -i -H 500 -W 250 -x 0 -y 0 )\" in
        'logout') swaymsg exit;;
        'sleep') loginctl suspend ;;
        'reboot') sudo /run/current-system/profile/sbin/reboot ;;
        'shutdown') sudo /run/current-system/profile/sbin/halt ;;
        *) exit 1 ;;
esac" "\nEOF"))
        (chmod #$output #o755))))

(define %bin:lock
  (computed-file "bin-lock"
    #~(begin
        (system (string-append #$coreutils "/bin/cat << \"EOF\" >" #$output "\n" "\
#!/usr/bin/env bash
if ! ps aux | grep -v grep | grep swaylock; then
#        if [[ $(playerctl status) != \"Playing\" ]]; then
#                swayidle -w timeout 600 'loginctl suspend' &
#                SUSPEND=$!
#        fi
        swayidle -w timeout 15 'swaymsg \"output * dpms off\"' resume 'swaymsg \"output * dpms on\"' &
        SCREENOFF=$!
        swaylock
        kill $SUSPEND $SCREENOFF
fi" "\nEOF"))
        (chmod #$output #o755))))
