(define-module (cobra home configs)
  #:use-module (guix gexp)
  #:export (%waybar:style
            %waybar:config
            %config:swaylock
            %config:sway
            %config:mpv
            %config:mako
            %config:foot
            %config:colorscheme
            %config:hyfetch
            %config:neofetch))

(define %waybar:style
  (plain-file "waybar-style" "\
/* =============================================================================
 *
 * Waybar configuration
 *
 * Configuration reference: https://github.com/Alexays/Waybar/wiki/Configuration
 *
 * =========================================================================== */

/* -----------------------------------------------------------------------------
 * Keyframes
 * -------------------------------------------------------------------------- */

@keyframes blink-warning {
    70% {
        color: white;
    }

    to {
        color: white;
        background-color: orange;
    }
}

@keyframes blink-critical {
    70% {
      color: white;
    }

    to {
        color: white;
        background-color: red;
    }
}


/* -----------------------------------------------------------------------------
 * Base styles
 * -------------------------------------------------------------------------- */

/* Reset all styles */
* {
    border: none;
    border-radius: 0;
    min-height: 0;
    margin: 0;
    padding: 0;
}

/* The whole bar */
#waybar {
    background: transparent;
    color: #473034;
    font-family: 'Fira Mono';
    font-size: 14px;
}

/* Each module */
#battery,
#clock,
#cpu,
#memory,
#mode,
#network,
#pulseaudio,
#temperature,
#tray {
    padding-left: 10px;
    padding-right: 10px;
}


/* -----------------------------------------------------------------------------
 * Module styles
 * -------------------------------------------------------------------------- */

#battery {
    background-color: #FF99C8;
    color: black;
    animation-timing-function: linear;
    animation-iteration-count: infinite;
    animation-direction: alternate;
    border-top-right-radius: 15px;
    border-bottom-right-radius: 15px;
}

#battery.warning {
    color: orange;
    font-weight: bold;
}

#battery.critical {
    color: red;
    font-weight: bold;
}

#battery.warning.discharging {
    animation-name: blink-warning;
    animation-duration: 3s;
    font-weight: bold;
}

#battery.critical.discharging {
    animation-name: blink-critical;
    animation-duration: 2s;
}

#clock {
    font-weight: bold;
    color: black;
    text-shadow: -1px 0 white, 0 1px white, 1px 0 white, 0 -1px white;
    border-radius: 50%;
}

#cpu {
  /* No styles */
    background-color: #FCC5DC;
    color: black;
}

#cpu.warning {
    color: orange;
    font-weight: bold;
}

#cpu.critical {
    color: red;
    font-weight: bold;
}

#memory {
    animation-timing-function: linear;
    animation-iteration-count: infinite;
    animation-direction: alternate;
    background-color: #FBDBE6;
    color: black;
}

#memory.warning {
    color: orange;
    font-weight: bold;
}

#memory.critical {
    color: red;
    animation-name: blink-critical;
    animation-duration: 2s;
    font-weight: bold;
}

#mode {
    background: #64727D;
    border-top: 2px solid white;
    /* To compensate for the top border and still have vertical centering */
    padding-bottom: 2px;
    margin-left: 10px;
}

#network {
    /* No styles */
    background-color: #F9F1F0;
    color: black;
    border-top-left-radius: 15px;
    border-bottom-left-radius: 15px;
}

#network.disconnected {
/*    color: orange;*/
}

#pulseaudio {
    /* No styles */
}

#pulseaudio.muted {
    /* No styles */
}

#temperature {
    /* No styles */
    background-color: #FEAFD2;
    color: black;
}

#temperature.critical {
    color: red;
}

#tray {
    /* No styles */
}

#window {
    font-weight: bold;
    padding-left: 5px;
}

#workspaces button {
    border: 2px solid white;
    border-radius: 15px;
    padding-left: 10px;
    padding-right: 10px;
    color: white;
}

#workspaces button.focused {
    background-color: #FF99C8;
}

#workspaces button.urgent {
    border-color: #FF99C8;
    color: #FF99C8;
}"))

(define %waybar:config
  (plain-file "waybar-config" "\
// =============================================================================
//
// Waybar configuration
//
// Configuration reference: https://github.com/Alexays/Waybar/wiki/Configuration
//
// =============================================================================

{
    // -------------------------------------------------------------------------
    // Global configuration
    // -------------------------------------------------------------------------

    \"layer\": \"top\",

    \"position\": \"top\",

    // If height property would be not present, it'd be calculated dynamically
    \"height\": 30,

    \"modules-left\": [
        \"sway/workspaces\",
        \"sway/window\",
        \"sway/mode\",
    ],
//    \"modules-center\": [
//    ],
    \"modules-right\": [
        \"network\",
        \"memory\",
        \"cpu\",
        \"temperature\",
        \"custom/keyboard-layout\",
        \"battery\",
        \"tray\",
        \"clock#date\",
        \"clock#time\"
    ],


    // -------------------------------------------------------------------------
    // Modules
    // -------------------------------------------------------------------------

    \"battery\": {
        \"interval\": 10,
        \"states\": {
            \"warning\": 30,
            \"critical\": 15
        },
        // Connected to AC
        \"format\": \"{icon} {capacity}%\", // Icon: bolt
        // Not connected to AC
        \"format-discharging\": \"{icon} {capacity}%\",
        \"format-icons\": [
                \"\",
                \"\",
                \"\",
                \"\",
                \"\"
        ],
        \"tooltip\": true
    },

    \"clock#time\": {
        \"interval\": 1,
        \"format\": \"{:%H:%M:%S}\",
        \"tooltip\": false
    },

    \"clock#date\": {
      \"interval\": 10,
      \"format\": \"{:%Y-%m-%d}\", // Icon: calendar-alt
      \"tooltip-format\": \"{:%e %B %Y}\"
    },

    \"cpu\": {
        \"interval\": 5,
        \"format\": \" {usage}%\", // Icon: cpu
        \"states\": {
          \"warning\": 70,
          \"critical\": 90
        }
    },

    \"memory\": {
        \"interval\": 5,
        \"format\": \" {}%\", // Icon: microchip
        \"states\": {
            \"warning\": 70,
            \"critical\": 90
        }
    },

    \"network\": {
        \"interval\": 5,
        \"format-wifi\": \" {signalStrength}%\", // Icon: wifi
        \"format-ethernet\": \" {ifname}: {ipaddr}/{cidr}\", // Icon: ethernet
        \"format-disconnected\": \"⚠ Disconnected\",
        \"tooltip-format\": \"{ifname}: {ipaddr}\"
    },

    \"sway/mode\": {
        \"format\": \"<span style=\\\"italic\\\">  {}</span>\", // Icon: expand-arrows-alt
        \"tooltip\": false
    },

    \"sway/window\": {
        \"format\": \"{}\",
        \"max-length\": 120
    },

    \"sway/workspaces\": {
        \"all-outputs\": false,
        \"disable-scroll\": true,
        \"format\": \"{name}\",
        \"format-icons\": {
//            \"1:www\": \"龜\", // Icon: firefox-browser
//            \"2:mail\": \"\", // Icon: mail
//            \"3:editor\": \"\", // Icon: code
//            \"4:terminals\": \"\", // Icon: terminal
//            \"5:portal\": \"\", // Icon: terminal
//            \"urgent\": \"\",
            \"focused\": \"\",
            \"default\": \"\"
        }
    },

    //\"pulseaudio\": {
    //    //\"scroll-step\": 1,
    //    \"format\": \"{icon}  {volume}%\",
    //    \"format-bluetooth\": \"{icon}  {volume}%\",
    //    \"format-muted\": \"\",
    //    \"format-icons\": {
    //        \"headphones\": \"\",
    //        \"handsfree\": \"\",
    //        \"headset\": \"\",
    //        \"phone\": \"\",
    //        \"portable\": \"\",
    //        \"car\": \"\",
    //        \"default\": [\"\", \"\"]
    //    },
    //    \"on-click\": \"pavucontrol\"
    //},

    \"temperature\": {
      \"critical-threshold\": 80,
      \"interval\": 5,
      \"format\": \"{icon} {temperatureC}°C\",
      \"format-icons\": [
          \"\", // Icon: temperature-empty
          \"\", // Icon: temperature-quarter
          \"\", // Icon: temperature-half
          \"\", // Icon: temperature-three-quarters
          \"\"  // Icon: temperature-full
      ],
      \"tooltip\": true
    },

    \"tray\": {
        \"icon-size\": 21,
        \"spacing\": 10
    }

}"))

(define %config:swaylock
  (plain-file "config-swaylock" "\
ignore-empty-password
show-failed-attempts
hide-keyboard-layout
disable-caps-lock-text
bs-hl-color=d88f96
inside-color=4730327f
inside-clear-color=dbbc2f7f
inside-ver-color=1961e67f
inside-wrong-color=d88f967f
key-hl-color=c6888d
line-color=fcf5f3
line-clear-color=fcf5f3
line-ver-color=fcf5f3
line-wrong-color=fcf5f3
ring-color=b3787e
ring-clear-color=f3d64d
ring-ver-color=4545db
ring-wrong-color=d88f96
separator-color=fcf5f3
text-color=fcf5f3
text-clear-color=fcf5f3
text-ver-color=fcf5f3
text-wrong-color=fcf5f3
image=~/.config/sway/background.png
font=\"Fira Code\""))

(define %config:sway
  (plain-file "config-sway" "\
# SWAY CONFIG


# Variables

set $mod Mod4
set $menu wofi --show drun -i -H 500 -W 450
set $term foot

set $WOBSOCK $XDG_RUNTIME_DIR/wob.sock

floating_modifier $mod normal
default_border pixel 2
client.focused #FF99C8 #FF99C8 #F9F1F0 #F9F1F0
client.unfocused #FCC5DC #FCC5DC #ffffff
client.focused_inactive #FCC5DC #FCC5DC #ffffff
gaps inner 10
gaps outer 0
font pango: Fira Code 12

# No gaps if there's only 1 window
smart_gaps on

# Hides the borders if theres only 1 window
hide_edge_borders smart

# Output configuration
output * {
        bg ~/.config/sway/background.png fill
}

output LVDS-1 pos 640 1080
output DP-1 pos 1280 0
output VGA-1 pos 0 56

# Input configuration
input type:touchpad {
        dwt disabled
        scroll_method edge
}

# Key bindings
bindsym $mod+Return exec $term
bindsym $mod+Shift+Return exec $term -a float
bindsym $mod+q kill
bindsym $mod+F4 wkill
bindsym $mod+d exec $menu
bindsym $mod+Shift+c reload
bindsym mod1+shift+w exec setbg -s
bindsym $mod+Shift+l exec ~/.local/bin/lock
bindsym $mod+Shift+q exec ~/.local/bin/logout-wofi

# Moving around:

# Move your focus around
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# Move the focused window with the same, but add Shift
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

#resize
bindsym $mod+mod1+Left exec swaymsg resize shrink right 10px || swaymsg resize grow left 10px
bindsym $mod+mod1+Down exec swaymsg resize grow down 10px || swaymsg resize shrink up 10px
bindsym $mod+mod1+Up exec swaymsg resize shrink down 10px || swaymsg resize grow up 10px
bindsym $mod+mod1+Right exec swaymsg resize grow right 10px || swaymsg resize shrink left 10px

# Workspaces:

# Switch to workspace
bindsym $mod+1 workspace number 1
bindsym $mod+2 workspace number 2
bindsym $mod+3 workspace number 3
bindsym $mod+4 workspace number 4
bindsym $mod+5 workspace number 5
bindsym $mod+6 workspace number 6
bindsym $mod+7 workspace number 7
bindsym $mod+8 workspace number 8
bindsym $mod+9 workspace number 9
bindsym $mod+0 workspace number 10
bindsym $mod+mod1+1 workspace number 11
bindsym $mod+mod1+2 workspace number 12
bindsym $mod+mod1+3 workspace number 13
bindsym $mod+mod1+4 workspace number 14
bindsym $mod+mod1+5 workspace number 15
bindsym $mod+mod1+6 workspace number 16
bindsym $mod+mod1+7 workspace number 17
bindsym $mod+mod1+8 workspace number 18
bindsym $mod+mod1+9 workspace number 19
bindsym $mod+mod1+0 workspace number 20
# Move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number 1
bindsym $mod+Shift+2 move container to workspace number 2
bindsym $mod+Shift+3 move container to workspace number 3
bindsym $mod+Shift+4 move container to workspace number 4
bindsym $mod+Shift+5 move container to workspace number 5
bindsym $mod+Shift+6 move container to workspace number 6
bindsym $mod+Shift+7 move container to workspace number 7
bindsym $mod+Shift+8 move container to workspace number 8
bindsym $mod+Shift+9 move container to workspace number 9
bindsym $mod+Shift+0 move container to workspace number 10
bindsym $mod+Shift+mod1+1 move container to workspace number 11
bindsym $mod+Shift+mod1+2 move container to workspace number 12
bindsym $mod+Shift+mod1+3 move container to workspace number 13
bindsym $mod+Shift+mod1+4 move container to workspace number 14
bindsym $mod+Shift+mod1+5 move container to workspace number 15
bindsym $mod+Shift+mod1+6 move container to workspace number 16
bindsym $mod+Shift+mod1+7 move container to workspace number 17
bindsym $mod+Shift+mod1+8 move container to workspace number 18
bindsym $mod+Shift+mod1+9 move container to workspace number 19
bindsym $mod+Shift+mod1+0 move container to workspace number 20

# Layout stuff:
bindsym $mod+b splith
bindsym $mod+v splitv

# Switch the current container between different layout styles
bindsym $mod+t layout toggle split
bindsym $mod+Shift+t layout tabbed
bindsym $mod+Shift+s layout stacking

# Make the current focus fullscreen
bindsym $mod+f fullscreen

# Toggle the current focus between tiling and floating mode
bindsym $mod+space floating toggle

# Swap focus between the tiling area and the floating area
bindsym $mod+Shift+space focus mode_toggle

# Move focus to the parent container
bindsym $mod+a focus parent

# Sticky
bindsym $mod+s sticky toggle
# Scratchpad:

bindsym $mod+bracketleft move scratchpad
bindsym $mod+bracketright scratchpad show

# Tor Browser
bindsym $mod+w exec ~/.local/bin/torbrowser


# Resizing containers:

mode \"resize\" {
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    # Return to default mode
    bindsym Return mode \"default\"
    bindsym Escape mode \"default\"
}


# Media Keys
bindsym XF86AudioLowerVolume exec pamixer -d 5 && pamixer --get-volume > $WOBSOCK
bindsym XF86AudioRaiseVolume exec pamixer -i 5 && pamixer --get-volume > $WOBSOCK

bindsym XF86MonBrightnessUp exec brightnessctl s +5%
bindsym XF86MonBrightnessDown exec brightnessctl s 5%-

bindsym XF86AudioStop exec mpc stop
bindsym XF86AudioPlay exec mpc toggle
bindsym XF86AudioNext exec mpc next
bindsym XF86AudioPrev exec mpc prev

bindsym $mod+r exec ~/.local/bin/wf-shadow -r LVDS-1
bindsym $mod+g exec ~/.local/bin/wf-shadow -r DP-1

bindsym XF86RotateWindows exec ~/.local/bin/rotate
bindsym XF86ScreenSaver exec ~/.local/bin/lock
bindsym Print exec ~/.local/bin/screenshot -w
bindsym Pause exec ~/.local/bin/logout-wofi

# Mumble

bindsym $mod+z exec mumble rpc starttalking
bindsym --release $mod+z exec mumble rpc stoptalking

# Rules
for_window [app_id=\"float\"] floating enable;

exec swayidle -w timeout 290 'brightnessctl -s; brightnessctl s 50%-' resume 'brightnessctl -r'
exec swayidle -w timeout 300 '~/.local/bin/lock'
exec rm -f $WOBSOCK && mkfifo $WOBSOCK && tail -f $WOBSOCK | wob

# Audio
exec ~/.local/bin/start-pipewire

exec shepherd
exec mpDris2
exec mpdscribble -D
exec mako
exec udiskie

# this shit is so jank but i need this keybind so
bindsym $mod+m exec sudo /usr/bin/refresh-hmd

# they call me the ethernet stealer
bindsym $mod+Shift+m exec bash -c 'sudo macchanger -A $(ls /sys/devices/pci0000:00/*/net/)'

bar {
        swaybar_command waybar
    }
}

include /etc/sway/config.d/*"))

(define %config:mpv
  (plain-file "config-mpv" "\
ytdl-format=\"bestvideo[height<=?480][fps<=?30][vcodec!=?vp9]+bestaudio/best\"
user-agent=\"Mozilla/5.0\"


msg-level=ffmpeg=fatal
vo=gpu
hwdec=vaapi
hwdec-codecs=all
video-sync=display-resample"))

(define %config:mako
  (plain-file "config-mako" "\
default-timeout=8000
# configversion: b2dba52da9a482ccd1a8170517e502c5
background-color=#2e2016
text-color=#ffc8dd
border-color=#473032
layer=overlay
group-by=app-name

[urgency=low]
default-timeout=5000
background-color=#222222
text-color=#888888

[urgency=high]
default-timeout=10000
background-color=#900000
text-color=#ffffff
background-color=#ff0000"))

(define %config:foot
  (plain-file "config-foot" "\
# -*- conf -*-
# shell=$SHELL (if set, otherwise user's default shell from /etc/passwd)
# term=foot (or xterm-256color if built with -Dterminfo=disabled)
# login-shell=no

# app-id=foot
# title=foot
# locked-title=no

 font= Fira Code:size=8

# font-bold=<bold variant of regular font>
# font-italic=<italic variant of regular font>
# font-bold-italic=<bold+italic variant of regular font>
# line-height=<font metrics>
# letter-spacing=0
# horizontal-letter-offset=0
# vertical-letter-offset=0
# underline-offset=<font metrics>
# box-drawings-uses-font-glyphs=no

dpi-aware=false

# initial-window-size-pixels=700x500  # Or,
# initial-window-size-chars=<COLSxROWS>
# initial-window-mode=windowed
pad=10x10                             # optionally append 'center'
# resize-delay-ms=100

# notify=notify-send -a ${app-id} -i ${app-id} ${title} ${body}

bold-text-in-bright=no
# word-delimiters=,│`|:\"'()[]{}<>
# selection-target=primary
# workers=<number of logical CPUs>

[bell]
# urgent=no
# notify=no
# command=
# command-focused=no

[scrollback]
# lines=1000
# multiplier=3.0
# indicator-position=relative
# indicator-format=

[url]
# launch=xdg-open ${url}
# label-letters=sadfjklewcmpgh
# osc8-underline=url-mode
# protocols=http, https, ftp, ftps, file, gemini, gopher
# uri-characters=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.,~:;/?#@!$&%*+=\"'()[]

[cursor]
# style=block
# color=<inverse foreground/background>
blink=yes
# beam-thickness=1.5
# underline-thickness=<font underline thickness>

[mouse]
hide-when-typing=yes
# alternate-scroll-mode=yes

[colors]
alpha=0.7
#Default
foreground=f4e5e7
background=17100b
regular0=2e2016  # black
regular1=ff99c8  # red
regular2=16af38  # green
regular3=dbbc2f  # yellow
regular4=1961e6  # blue
regular5=b3787e  # magenta
regular6=32cbe6  # cyan
regular7=f4e5e7  # white
bright0=473032   # bright black
bright1=fcc5dc   # bright red
bright2=27d64d   # bright green
bright3=f3d64d   # bright yellow
bright4=4545db   # bright blue
bright5=c6888d   # bright magenta
bright6=67cbe6   # bright cyan
bright7=fcf5f3   # bright white

## dimmed colors (see foot.ini(5) man page)
# dim0=<not set>
# ...
# dim7=<not-set>

## The remaining 256-color palette
#16 = <256-color palette #16>
# ...
#255 = <256-color palette #255>

## Misc colors
#selection-foreground=<inverse foreground/background>
#selection-background=<inverse foreground/background>
#jump-labels=<regular0> <regular3>
#urls=<regular3>
#scrollback-indicator=<regular0> <bright4>

[csd]
# preferred=server
# size=26
# font=<primary font>
# color=<foreground color>
# hide-when-typing=no
# border-width=0
# border-color=<csd.color>
# button-width=26
# button-color=<background color>
# button-minimize-color=<regular4>
# button-maximize-color=<regular2>
# button-close-color=<regular1>

[key-bindings]
scrollback-up-page=none
scrollback-up-half-page=Mod1+Shift+e
scrollback-up-line=Mod1+e
scrollback-down-page=none
scrollback-down-half-page=Mod1+Shift+n
scrollback-down-line=Mod1+n
#clipboard-copy=Control+Shift+c XF86Copy
#clipboard-paste=Control+Shift+v XF86Paste
# primary-paste=Shift+Insert
# search-start=Control+Shift+r
# font-increase=Control+plus Control+equal Control+KP_Add
# font-decrease=Control+minus Control+KP_Subtract
# font-reset=Control+0 Control+KP_0
# spawn-terminal=Control+Shift+n
# minimize=none
# maximize=none
# fullscreen=none
# pipe-visible=[sh -c \"xurls | fuzzel | xargs -r firefox\"] none
# pipe-scrollback=[sh -c \"xurls | fuzzel | xargs -r firefox\"] none
# pipe-selected=[xargs -r firefox] none
# show-urls-launch=Control+Shift+u
# show-urls-copy=none
# show-urls-persistent=none
# noop=none

[search-bindings]
# cancel=Control+g Control+c Escape
# commit=Return
# find-prev=Control+r
# find-next=Control+s
# cursor-left=Left Control+b
# cursor-left-word=Control+Left Mod1+b
# cursor-right=Right Control+f
# cursor-right-word=Control+Right Mod1+f
# cursor-home=Home Control+a
# cursor-end=End Control+e
# delete-prev=BackSpace
# delete-prev-word=Mod1+BackSpace Control+BackSpace
# delete-next=Delete
# delete-next-word=Mod1+d Control+Delete
# extend-to-word-boundary=Control+w
# extend-to-next-whitespace=Control+Shift+w
# clipboard-paste=Control+v Control+Shift+v Control+y XF86Paste
# primary-paste=Shift+Insert

[url-bindings]
# cancel=Control+g Control+c Control+d Escape
# toggle-url-visible=t

[text-bindings]
# \\x03=Mod4+c  # Map Super+c -> Ctrl+c

[mouse-bindings]
# selection-override-modifiers=Shift
# primary-paste=BTN_MIDDLE
# select-begin=BTN_LEFT
# select-begin-block=Control+BTN_LEFT
# select-extend=BTN_RIGHT
# select-extend-character-wise=Control+BTN_RIGHT
# select-word=BTN_LEFT-2
# select-word-whitespace=Control+BTN_LEFT-2
# select-row=BTN_LEFT-3

# vim: ft=dosini"))

(define %config:colorscheme
  (plain-file "config-colorscheme" "\
#!/bin/sh
foreground=f4e5e7
background=17100b
regular0=2e2016  # black
regular1=ff99c8  # red
regular2=16af38  # green
regular3=dbbc2f  # yellow
regular4=1961e6  # blue
regular5=b3787e  # magenta
regular6=32cbe6  # cyan
regular7=f4e5e7  # white
bright0=473032   # bright black
bright1=fcc5dc   # bright red
bright2=27d64d   # bright green
bright3=f3d64d   # bright yellow
bright4=4545db   # bright blue
bright5=c6888d   # bright magenta
bright6=67cbe6   # bright cyan
bright7=fcf5f3   # bright white

if [[ \"$TERM\" = \"vt102\" || \"$TERM\" = \"linux\" ]]; then
        printf \"\\e]P$background\\e]P1$regular1\\e]P2$regular2\\e]P3$regular3\\e]P4$regular4\\e]P5$regular5\\e]P6$regular6\\e]P7$foreground\\e]P8$bright0\\e]P9$bright1\\e]PA$bright2\\e]PB$bright3\\e]PC$bright4\\e]PD$bright5\\e]PE$bright6\\e]PF$bright7\"
fi"))

(define %config:hyfetch
  (plain-file "config-hyfetch" "{\"preset\": \"transgender\", \"mode\": \"rgb\", \"light_dark\": \"dark\", \"lightness\": 0.65, \"color_align\": {\"mode\": \"horizontal\", \"custom_colors\": [], \"fore_back\": null}}"))

(define %config:neofetch
  (plain-file "config-neofetch" "\
print_info() {
    info title
    info underline
    info \" OS\" distro
    info \"󰌢 Host\" model
    info \" Kernel\" kernel
    info \" Uptime\" uptime
    info \" Packages\" packages
    info \" Shell\" shell
    info \"󰍹 Resolution\" resolution
    info \"DE\" de
    info \" WM\" wm
    info \" Terminal\" term
    [[ \"$player\" ]] && prin \"Music Player\" \"$player\"
    info \"󰀥 Song\" song
    info \" CPU\" cpu
    info \"CPU Usage\" cpu_usage
    info \"󰢮 GPU\" gpu
    info \" GPU Driver\" gpu_driver  # Linux/macOS only
    info \" Disk\" disk
    info \" Memory\" memory
    info cols
}
title_fqdn=\"off\"
kernel_shorthand=\"on\"
distro_shorthand=\"off\"
os_arch=\"on\"
uptime_shorthand=\"on\"
memory_percent=\"off\"
package_managers=\"on\"
shell_path=\"off\"
shell_version=\"on\"
speed_type=\"bios_limit\"
speed_shorthand=\"off\"
cpu_brand=\"on\"
cpu_speed=\"on\"
cpu_cores=\"logical\"
cpu_temp=\"off\"
gpu_brand=\"on\"
gpu_type=\"all\"
refresh_rate=\"off\"
gtk_shorthand=\"off\"
gtk2=\"on\"
gtk3=\"on\"
public_ip_host=\"http://ident.me\"
public_ip_timeout=2
de_version=\"off\"
disk_show=('/')
disk_subtitle=\"mount\"
disk_percent=\"on\"
music_player=\"auto\"
song_format=\"%artist% - %album% - %title%\"
song_shorthand=\"off\"
mpc_args=()
colors=(distro)
bold=\"on\"
underline_enabled=\"on\"
underline_char=\"-\"
separator=\":\"
block_range=(0 15)
color_blocks=\"on\"
block_width=3
block_height=1
col_offset=\"auto\"
bar_char_elapsed=\"-\"
bar_char_total=\"=\"
bar_border=\"on\"
bar_length=15
bar_color_elapsed=\"distro\"
bar_color_total=\"distro\"
cpu_display=\"off\"
memory_display=\"off\"
battery_display=\"off\"
disk_display=\"off\"
image_backend=\"ascii\"
image_source=\"auto\"
ascii_distro=\"auto\"
ascii_colors=(distro)
ascii_bold=\"on\"
image_loop=\"off\"
thumbnail_dir=\"${XDG_CACHE_HOME:-${HOME}/.cache}/thumbnails/neofetch\"
crop_mode=\"normal\"
crop_offset=\"center\"
image_size=\"auto\"
gap=3
yoffset=0
xoffset=0
background_color=
stdout=\"off\""))
