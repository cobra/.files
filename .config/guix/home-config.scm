(add-to-load-path "/etc/guix-config")
(add-to-load-path "/home/cobra/.config/guix")

(use-modules (gnu home)
             (gnu home services)
             (gnu home services mcron)
             (gnu home services desktop)
             (gnu home services shells)
             (gnu packages)
             (gnu packages gl)
             (gnu services)
             (gnu services dbus)
             (gnu services mcron)
             (guix gexp)

             ;; Deranged Guix Home Usage(TM)
             (cobra home)
             (cobra home configs)
             (cobra home bin)
             (cobra packages))

(define webfetch-job
  #~(job "*/1 * * * *"
         "PATH=/run/current-system/profile/bin:/home/cobra/.guix-home/profile/bin; hyfetch > /tmp/fetch; rsync --rsh=\"ssh -i /home/cobra/.ssh/id_fetch\" /tmp/fetch vern.cc:~/public_html/${HOSTNAME,,}"))

(home-environment
  (packages
    (map specification->package+output
         (list ;; Audio
               "alsa-utils"
               "mpd"
               "mpd-mpc"
               "mpdscribble"
               "mpdris2"
               "mumble"
               "pamixer"
               "pipewire"
               "playerctl"
               "pulseaudio"
               "pulsemixer"
               "wireplumber"

               ;; Browsers
               "icecat"
               "icedove"
               "lynx"
               "ungoogled-chromium-wayland"

               ;; CAD
               ;"cura"
               "freecad"
               "kicad"
               "kicad-doc"
               "kicad-footprints"
               "kicad-packages3d"
               "kicad-symbols"
               "kicad-templates"

               ;; Chat
               "irssi"
               "pinentry"
               "profanity"

               ;; Desktop
               "brightnessctl"
               "foot"
               "grim"
               "libnotify"
               "light"
               "mako"
               "slurp"
               "sway"
               "swayhide"
               "swayidle"
               "swaylock"
               "udiskie"
               "waybar"
               "wf-recorder"
               "wl-clipboard"
               "wlr-randr"
               "wob"
               "wofi"
               "xdg-desktop-portal"
               "xdg-desktop-portal-wlr"
               "xorg-server-xwayland"

               ;; Development
               "cmake"
               "fftw"
               "gcc-toolchain"
               "git"
               "git:send-email"
               "go"
               "guile-sdl2"
               "libftdi"
               "libgpiod"
               "libjaylink"
               "libusb"
               "make"
               "patchelf"
               "pciutils"
               "perl"
               "python"


               ;; DICT
               "dico"

               ;; Encryption
               "age"
               "age-keygen"
               "gnupg"
               "signify"
               ;"magic-wormhole"
               "pass-age"

               ;; Games
               "adanaxisgpl"
               "minetest"
               "red-eclipse"
               "supertuxkart"
               "teeworlds"
               "xonotic"

               ;; Multimedia
               "gimp"
               "handbrake"
               "imagemagick"
               "mpv"
               "ncmpcpp"
               "obs"
               "reptyr"
               "ripit"
               "vips"

               ;; Network
               "bind:utils"
               "i2pd"
               "net-tools"
               "netcat"
               "proxychains-ng"
               "socat"
               "tmate"
               "whois"
               "wireguard-tools"

               ;; Utilities
               "bc"
               "dialog"
               "emacs"
               "file"
               "htop"
               "hyfetch"
               "libqalculate"
               "lm-sensors"
               "monero-gui"
               "newsboat"
               "rsync"
               "scdoc"
               "unzip"
               "wget"
               "wireshark"
               "yt-dlp"
               "zip")))
  (services
    (list
      (service home-dbus-service-type)
      (service home-mcron-service-type
              (home-mcron-configuration
                (jobs (list webfetch-job))))
      (simple-service 'home-env
                      home-environment-variables-service-type
                      `(("LIBGL_DRIVERS_PATH" . ,(file-append mesa "/lib/dri"))
                        ("PRIV" . "sudo")
                        ("XDG_CURRENT_DESKTOP" . "sway")
                        ("WIFI" . "wlp2s0")
                        ("COPY_COMMAND" . "wl-copy")
                        ("PASTE_COMMAND" . "wl-paste")
                        ("UPDATE" . "guix pull --branch=master && reconf && hreconf")))
      (simple-service 'dotfiles
                      home-files-service-type
                      (list `(".bashrc" ,%home:bashrc)
                            `(".prompt" ,%home:prompt)
                            `(".devspec" ,%home:devspec)
                            `(".bash_profile" ,%home:bash-profile)

                            `(".local/bin/lock" ,%bin:lock)
                            `(".local/bin/logout-wofi" ,%bin:logout-wofi)
                            `(".local/bin/rotate" ,%bin:rotate)
                            `(".local/bin/screenshot" ,%bin:screenshot)
                            `(".local/bin/start-pipewire" ,%bin:start-pipewire)
                            `(".local/bin/suspend" ,%bin:suspend-wrap)
                            `(".local/bin/torbrowser" ,%bin:torbrowser)

                            `(".config/neofetch/config.conf" ,%config:neofetch)
                            `(".config/hyfetch.json" ,%config:hyfetch)
                            `(".config/colorscheme" ,%config:colorscheme)
                            `(".config/foot/foot.ini" ,%config:foot)
                            `(".config/mako/config" ,%config:mako)
                            `(".config/mpv/mpv.conf" ,%config:mpv)
                            `(".config/swaylock/config" ,%config:swaylock)
                            `(".config/sway/config" ,%config:sway)

                            `(".config/waybar/config" ,%waybar:config)
                            `(".config/waybar/style.css" ,%waybar:style))))))
