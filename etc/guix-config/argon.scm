(add-to-load-path (dirname (current-filename)))
(use-modules (gnu) (cobra os))

(operating-system
  (inherit %base-guix-system)
  (host-name "Argon")
  (mapped-devices (list
                    (mapped-device
                      (source (uuid "1ea1538d-57df-4b3d-a0fe-a052a04773c7"))
                      (target "fde")
                      (type luks-device-mapping))
                    (mapped-device
                      (source "grubcrypt")
                      (targets (list "grubcrypt-rootvol"))
                      (type lvm-device-mapping))))
  (file-systems (append (list
                          (file-system
                            (type "btrfs")
                            (mount-point "/")
                            (device (file-system-label "root"))
                            (flags '(no-atime))
                            (options "space_cache=v2")
                            (needed-for-boot? #t)
                            (dependencies mapped-devices)))
                       %base-file-systems)))
