(add-to-load-path (dirname (current-filename)))
(use-modules (gnu)
             (gnu services base)
             (gnu services shepherd)
             (gnu packages linux)
             (cobra os) (cobra lists))

(operating-system
  (inherit %base-guix-system)
  (kernel-arguments '("intel_iommu=on"
                      "kvm.ignore_msrs=1"
                      "log_buf_len=1M"
                      "iomem=relaxed"
                      "acpi=force"))
  (host-name "Bromine")
  (mapped-devices (list
                    (mapped-device
                      (source (uuid "58144730-a08b-4e26-89ce-791df910a212"))
                      (target "fde")
                      (type luks-device-mapping))
                    (mapped-device
                      (source "grubcrypt")
                      (targets (list "grubcrypt-rootvol"))
                      (type lvm-device-mapping))))
  (file-systems (append (list
                          (file-system
                            (type "btrfs")
                            (mount-point "/")
                            (device (file-system-label "root"))
                            (flags '(no-atime))
                            (options "space_cache=v2")
                            (needed-for-boot? #t)
                            (dependencies mapped-devices)))
                       %base-file-systems)))
