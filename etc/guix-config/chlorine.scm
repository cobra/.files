(add-to-load-path (dirname (current-filename)))
(use-modules (gnu) (cobra os))

(operating-system
  (inherit %base-guix-system)
  (host-name "Chlorine")
  (mapped-devices (list
                    (mapped-device
                      (source (uuid "dd33d02f-77f3-4bf1-bbf9-97f31648dccd"))
                      (target "fde")
                      (type luks-device-mapping))
                    (mapped-device
                      (source "grubcrypt")
                      (targets (list "grubcrypt-rootvol"))
                      (type lvm-device-mapping))))
  (file-systems (append (list
                          (file-system
                            (type "btrfs")
                            (mount-point "/")
                            (device (file-system-label "root"))
                            (flags '(no-atime))
                            (options "space_cache=v2")
                            (needed-for-boot? #t)
                            (dependencies mapped-devices)))
                       %base-file-systems)))
