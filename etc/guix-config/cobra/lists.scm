(add-to-load-path (canonicalize-path ".."))

(define-module (cobra lists)
  #:use-module (guix gexp)

  #:use-module (gnu)
  #:use-module (gnu services)
  #:use-module (gnu services authentication)
  #:use-module (gnu services base)
  #:use-module (gnu services cups)
  #:use-module (gnu services dbus)
  #:use-module (gnu services desktop)
  #:use-module (gnu services dict)
  #:use-module (gnu services linux)
  #:use-module (gnu services networking)
  #:use-module (gnu services security-token)
  #:use-module (gnu services ssh)
  #:use-module (gnu services virtualization)
  #:use-module (gnu services xorg)

  #:use-module (gnu packages certs)
  #:use-module (gnu packages cups)
  #:use-module (gnu packages dictionaries)
  #:use-module (gnu packages dns)
  #:use-module (gnu packages security-token)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages wordnet)
  #:use-module (gnu packages xdisorg)

  #:use-module (cobra packages)

  #:export (service-list
            package-list))

;; DICT
(define wordnet-handler
  (dicod-handler
    (name "wordnet")
    (module "dictorg")
    (options
      (list #~(string-append "dbdir=" #$wordnet)))))
(define vera-handler
  (dicod-handler
    (name "vera")
    (module "dictorg")
    (options
      (list #~(string-append "dbdir=" #$vera)))))

(define wordnet-db
  (dicod-database
    (name "wordnet")
    (complex? #t)
    (handler "wordnet")
    (options '("database=wn"))))
(define vera-db
  (dicod-database
    (name "vera")
    (complex? #t)
    (handler "vera")
    (options '("database=vera"))))

;; Kernel
(define v4l2loopback-config
  (plain-file "v4l2loopback.conf"
              (string-append
                "options v4l2loopback video_nr=6,7,8,9 "
                "card_label=loopback0,loopback1,loopback2,loopback3 "
                "exclusive_caps=1")))

(define %wacom-udev-rule
  (udev-rule
    "10-wacom.rules"
    (string-append "ACTION!=\"add|change\", "
                   "GOTO=\"wacom_end\"\n"
                   "ATTRS{id}==\"WACf*\" ENV{NAME}=\"Serial Wacom Tablet\", "
                   "ENV{ID_INPUT}=\"1\", "
                   "ENV{ID_INPUT_TABLET}=\"1\"\n"
                   "ATTRS{id}==\"FUJ*\" ENV{NAME}=\"Serial Wacom Tablet\", "
                   "ENV{ID_INPUT}=\"1\", "
                   "ENV{ID_INPUT_TABLET}=\"1\"\n"
                   "LABEL=\"wacom_end\"\n")))

(define %flipper-udev-rule
  (udev-rule
    "42-flipperzero.rules"
    (string-append "#Flipper Zero serial port\n"
                   "SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"0483\", "
                   "ATTRS{idProduct}==\"5740\", ATTRS{manufacturer}=="
                   "\"Flipper Devices Inc.\", TAG+=\"uaccess\", "
                   "GROUP=\"dialout\"\n"
                   "#Flipper Zero DFU\n"
                   "SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"0483\", "
                   "ATTRS{idProduct}==\"df11\", "
                   "ATTRS{manufacturer}==\"STMicroelectronics\", "
                   "TAG+=\"uaccess\", GROUP=\"dialout\"\n"
                   "#Flipper ESP32s2 BlackMagic\n"
                   "SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"303a\", "
                   "ATTRS{idProduct}==\"40??\", ATTRS{manufacturer}=="
                   "\"Flipper Devices Inc.\", TAG+=\"uaccess\", "
                   "GROUP=\"dialout\"\n"
                   "#Flipper U2F\n"
                   "SUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"0483\", "
                   "ATTRS{idProduct}==\"5741\", ATTRS{manufacturer}=="
                   "\"Flipper Devices Inc.\", ENV{ID_SECURITY_TOKEN}=\"1\"\n")))

(define service-list
  (cons*
    ;; Bluetooth
    (service bluetooth-service-type
             (bluetooth-configuration
               (auto-enable? #t)))

    ;; Desktop
    (service screen-locker-service-type
             (screen-locker-configuration
               (name "swaylock")
               (program (file-append swaylock "/bin/swaylock"))
               (allow-empty-password? #f)
               (using-pam? #t)
               (using-setuid? #f)))
    (service udisks-service-type)
    (extra-special-file
      "/usr/bin/refresh-hmd"
      (program-file "refresh-hmd"
                    #~(system*
                        "udevadm" "trigger" "-v"
                        "-p" "ID_VENDOR_ID=054c"
                        "-p" "ID_MODEL_ID=023d"
                        "-c" "change"
                        "-s" "block")))

    ;; DICT
    (service dicod-service-type
             (dicod-configuration
               (handlers (list wordnet-handler vera-handler))
               (databases (list wordnet-db vera-db %dicod-database:gcide))))

    ;; Kernel
    (service kernel-module-loader-service-type
             '("v4l2loopback" "spi-ch341-usb"))
    (simple-service 'v4l2loopback etc-service-type
                    (list `("modprobe.d/v4l2loopback.conf"
                            ,v4l2loopback-config)))

    ;; Network
    (service cups-service-type
             (cups-configuration
               (web-interface? #t)
               (extensions
                 (list cups-filters epson-inkjet-printer-escpr hplip-minimal))))
    (service network-manager-service-type)
    (service modem-manager-service-type)
    (service ntp-service-type)
    (service tor-service-type
             (tor-configuration
               (control-socket? #t)
               (config-file (plain-file "tor-config"
                                        "HTTPTunnelPort 127.0.0.1:9250"))))
    (service wpa-supplicant-service-type)

    ;; System
    (service dbus-root-service-type)
    (service elogind-service-type)
    (service gpm-service-type)
    (service openssh-service-type
             (openssh-configuration
               (permit-root-login 'prohibit-password)
               (password-authentication? #f)))
    (service pcscd-service-type)
    (udev-rules-service 'nitrokey libnitrokey)

    ;; Virtualization
    (service libvirt-service-type
             (libvirt-configuration
               (unix-sock-group "libvirt")))
    (service virtlog-service-type)
    (modify-services %base-services
      (udev-service-type config =>
        (udev-configuration (inherit config)
                            (rules (append (udev-configuration-rules config)
                                           (list %wacom-udev-rule
                                                 %flipper-udev-rule))))))))

(define package-list
  (append
    (map specification->package+output
         (list ;; Bluetooth
               "blueman"
               "bluez"

               ;; DVD
               "libdvdcss"
               "libdvdread"

               ;; Fonts
               "font-awesome"
               "font-fira-code"
               "font-fira-mono"
               "font-fira-sans"
               "font-google-noto"
               "font-google-noto-sans-cjk"
               "font-google-noto-serif-cjk"
               "font-liberation"
               "font-openmoji"

               ;; Graphics
               "intel-vaapi-driver"
               "mesa"
               "mesa-utils"

               ;; Kernel
               "v4l2loopback-linux-module"

               ;; Network
               "cups"
               "hplip-minimal"
               "macchanger"
               "modem-manager"
               "torsocks"
               "tor"

               ;; System
               "acpi"
               "ccid"
               "cryptsetup"
               "curl"
               "dconf"
               "dosfstools"
               "exfatprogs"
               "fuse@2"
               "pcsc-lite"
               "pcsc-tools"
               "libinput"
               "libnitrokey"
               "libwacom"
               "lvm2"
               "openssh"
               "openssl"
               "udisks"
               "xf86-input-wacom"

               ;; Utilities
               "autoconf"
               "automake"
               "gumbo-parser"
               "libtool"
               "libxml2"
               "pkg-config"
               "vim"

               ;; Virtualization
               "libvirt"
               "virt-manager"
               "qemu"))
    (cons* (list isc-bind "utils")
           spi-ch341-usb-module
           font-nerd-fonts-fira-code
           font-nerd-fonts-fira-mono
           %base-packages)))
