(add-to-load-path (canonicalize-path ".."))

(define-module (cobra os)
  #:use-module (gnu)
  #:use-module (gnu system nss)
  #:use-module (gnu packages linux)
  #:use-module (guix gexp)
  #:use-module (cobra packages)
  #:use-module (cobra lists)
  #:export (%base-guix-system))

(define sudoers
  (plain-file "sudoers" "\
root ALL=(ALL) ALL
%wheel ALL=(ALL) ALL
Cmnd_Alias WITHOUTPW = /run/current-system/profile/sbin/halt, /run/current-system/profile/sbin/reboot, /usr/bin/refresh-hmd, /run/current-system/profile/bin/macchanger
Defaults!WITHOUTPW !authenticate
"))

(define %base-guix-system
  (operating-system
    (kernel linux-libre)
    (kernel-loadable-modules (list v4l2loopback-linux-module
                                   spi-ch341-usb-module))
    (kernel-arguments '("intel_iommu=on"
                        "kvm.ignore_msrs=1"
                        "log_buf_len=1M"
                        "iomem=relaxed" ))
                        ;"modprobe.blacklist=ch341"))
    (keyboard-layout (keyboard-layout "us" "altgr-intl"))
    (bootloader
      (bootloader-configuration
        (bootloader (bootloader
                      (inherit grub-bootloader)
                      (installer #~(const #t))))
        (keyboard-layout keyboard-layout)))
    (host-name "Noble-Gas")
    (file-systems %base-file-systems)
    (users (append (list
                     (user-account
                       (name "cobra")
                       (comment "Skylar \"The Cobra\" Astaroth")
                       (group "users")
                       (supplementary-groups '("audio"
                                               "cdrom"
                                               "dialout"
                                               "floppy"
                                               "input"
                                               "kvm"
                                               "libvirt"
                                               "lp"
                                               "netdev"
                                               "tape"
                                               "tor"
                                               "video"
                                               "wheel"))))
                   %base-user-accounts))
    (packages package-list)
    (timezone "America/New_York")
    (locale "en_US.utf8")
    (name-service-switch %mdns-host-lookup-nss)
    (services service-list)
    (sudoers-file sudoers)))
