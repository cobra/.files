(define-module (cobra packages)
  #:use-module (guix)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system font)
  #:use-module (guix build-system linux-module)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages gl))

(define-public mesa-i915
  (package/inherit mesa
    (name "mesa-i915")
    (arguments
      (substitute-keyword-arguments (package-arguments mesa)
        ((#:configure-flags configure-flags '())
         #~(list "-Dgallium-drivers=svga,swrast,i915"
                 "-Dplatforms=x11,wayland"
                 "-Dglx=dri"
                 "-Dosmesa=true"
                 "-Dgallium-xa=enabled"
                 "-Dgles2=enabled"
                 "-Dgbm=enabled"
                 "-Dshared-glapi=enabled"
                 "-Dvulkan-drivers=intel,amd"
                 "-Dvulkan-layers=device-select,overlay"
                 "-Dvideo-codecs=vc1dec,h264dec,h264enc,h265dec,h265enc"
                 "-Dbuild-tests=true"
                 "-Dllvm=enabled"))))))

;; Taken and modified from issue #44575

(define-public font-nerd-fonts-fira-code
  (package
    (name "font-nerd-fonts-fira-code")
    (version "3.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
              "https://github.com/ryanoasis/nerd-fonts/releases/download/v"
              version "/FiraCode.tar.xz"))
       (sha256
        (base32
         "1brqps4j1n57l4y25m38mjcsxhl1jdhiwf5dgi482zbfi89yx5l7"))))
    (build-system font-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'install 'make-files-writable
           (lambda _
             (for-each
              make-file-writable
              (find-files "." ".*\\.(otf|otc|ttf|ttc)$"))
             #t)))))
    (home-page "https://www.nerdfonts.com/")
    (synopsis "Iconic font aggregator, collection, and patcher")
    (description
     "Nerd Fonts patches developer targeted fonts with a high number
of glyphs (icons). Specifically to add a high number of extra glyphs
from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
and others. Fira Code font.")
    (license license:expat)))

(define-public font-nerd-fonts-fira-mono
  (package
    (name "font-nerd-fonts-fira-mono")
    (version "3.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
              "https://github.com/ryanoasis/nerd-fonts/releases/download/v"
              version "/FiraMono.tar.xz"))
       (sha256
        (base32
         "0gf2vd9zz33hjkm2l1nyk458j561x7mva6p2nc3ja92156q99ci2"))))
    (build-system font-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'install 'make-files-writable
           (lambda _
             (for-each
              make-file-writable
              (find-files "." ".*\\.(otf|otc|ttf|ttc)$"))
             #t)))))
    (home-page "https://www.nerdfonts.com/")
    (synopsis "Iconic font aggregator, collection, and patcher")
    (description
     "Nerd Fonts patches developer targeted fonts with a high number
of glyphs (icons). Specifically to add a high number of extra glyphs
from popular ‘iconic fonts’ such as Font Awesome, Devicons, Octicons,
and others. Fira Mono font.")
    (license license:expat)))




(define-public spi-ch341-usb-module
  (package
    (name "spi-ch341-usb-module")
    (version "cbf663acd9b15255999e447a3e62b82c17ba9fa5")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/dimich-dmb/spi-ch341-usb")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1h68driaq76khabnwbw72ik7bl2kv5rlb9wlam894qa99yp3wypx"))))
    (build-system linux-module-build-system)
    (arguments
     (list #:tests? #f))                ; no test suite
    (home-page "https://github.com/dimich-dmb/spi-ch341-usb")
    (synopsis "Linux kernel driver for CH341A USB to SPI and GPIO adapters")
    (description
     "This package provides the @code{spi-ch341-usb} kernel module")
    (license license:gpl2)))
