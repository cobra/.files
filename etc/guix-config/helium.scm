(add-to-load-path (dirname (current-filename)))
(use-modules (gnu) (gnu services) (cobra os) (cobra lists))

(operating-system
  (inherit %base-guix-system)
  (host-name "Helium")
  (mapped-devices (list
                    (mapped-device
                      (source (uuid "aab7884f-f1ba-4396-9936-d61b5e7e06f4"))
                      (target "fde")
                      (type luks-device-mapping))
                    (mapped-device
                      (source "grubcrypt")
                      (targets (list "grubcrypt-rootvol"))
                      (type lvm-device-mapping))))
  (file-systems (append (list
                          (file-system
                            (type "btrfs")
                            (mount-point "/")
                            (device (file-system-label "root"))
                            (flags '(no-atime))
                            (options "space_cache=v2")
                            (needed-for-boot? #t)
                            (dependencies mapped-devices)))
                       %base-file-systems))
  (services
    (modify-services
      service-list
      (guix-service-type config =>
        (guix-configuration (inherit config)
                            (build-machines
                              (list #~(build-machine
                                        (name "guix.vern.cc")
                                        (systems (list "x86_64-linux"))
                                        (host-key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGSlfFh3RGuHkOxpAXbCT5ImCf4eChSHO3/5RuRM2NIL")
                                        (user "offloader")
                                        (port 1813)
                                        (private-key "/home/cobra/.ssh/id_offload"))))
                            (authorized-keys
                              (append (list (plain-file
                                              "vern-signing-key.pub"
                                              "(public-key (ecc (curve Ed25519) (q #2A123EF90B1B044E2B2B826A2822F0B595DF0710B5626541846A2733C85167B9#)))"))
                                      %default-authorized-guix-keys)))))))
