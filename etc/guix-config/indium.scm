(add-to-load-path (dirname (current-filename)))
(use-modules (gnu) (cobra os))

(operating-system
  (inherit %base-guix-system)
  (host-name "Indium")
  (mapped-devices (list
                    (mapped-device
                      (source (uuid "9420de71-ffdc-4bd3-ad5e-9a73e420004f"))
                      (target "fde")
                      (type luks-device-mapping))
                    (mapped-device
                      (source "grubcrypt")
                      (targets (list "grubcrypt-rootvol"))
                      (type lvm-device-mapping))))
  (file-systems (append (list
                          (file-system
                            (type "btrfs")
                            (mount-point "/")
                            (device (file-system-label "root"))
                            (flags '(no-atime))
                            (options "space_cache=v2")
                            (needed-for-boot? #t)
                            (dependencies mapped-devices)))
                       %base-file-systems))
  (swap-devices (list (swap-space
                        (target (uuid "9b269eaa-fbb8-4aeb-9a1f-caed2bc82d69"))))))
