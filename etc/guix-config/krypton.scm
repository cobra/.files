(add-to-load-path (dirname (current-filename)))
(use-modules (gnu) (cobra os))

(operating-system
  (inherit %base-guix-system)
  (host-name "Krypton")
  (mapped-devices (list
                    (mapped-device
                      (source (uuid "39ba4b5d-80e0-4ceb-add1-ccea1609b145"))
                      (target "fde")
                      (type luks-device-mapping))
                    (mapped-device
                      (source "grubcrypt")
                      (targets (list "grubcrypt-rootvol"))
                      (type lvm-device-mapping))))
  (file-systems (append (list
                          (file-system
                            (type "btrfs")
                            (mount-point "/")
                            (device (file-system-label "root"))
                            (flags '(no-atime))
                            (options "space_cache=v2")
                            (needed-for-boot? #t)
                            (dependencies mapped-devices)))
                       %base-file-systems))
  (swap-devices (list (swap-space
                        (target (uuid "bd949935-052e-4be2-ad42-df0adc5b7ded"))))))
