(add-to-load-path (dirname (current-filename)))
(use-modules (gnu) (cobra os))

(operating-system
  (inherit %base-guix-system)
  (host-name "Osmium")
  (keyboard-layout (keyboard-layout "us" "altgr-intl"))
  (bootloader
    (bootloader-configuration
      (bootloader grub-bootloader)
      (targets (list "/dev/sda"))
      (keyboard-layout keyboard-layout)))
  (mapped-devices (list
                    (mapped-device
                      (source "cf30")
                      (targets (list "cf30-rootvol"))
                      (type lvm-device-mapping))))
  (file-systems (append (list
                          (file-system
                            (type "btrfs")
                            (mount-point "/")
                            (device (file-system-label "root"))
                            (flags '(no-atime))
                            (options "space_cache=v2")
                            (needed-for-boot? #t)
                            (dependencies mapped-devices)))
                       %base-file-systems))
  (swap-devices (list (swap-space
                        (target (uuid "790eff60-7202-4922-b69f-d433bc4cd3ef"))))))

